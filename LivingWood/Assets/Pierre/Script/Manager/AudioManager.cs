﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    /// <summary>
    /// this script play ALL the sound    /// 
    /// </summary>

    public Sound[] sounds;

    public static AudioManager instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        // on ajoute des sound source à chaque objet de la classe sound
        foreach (Sound s in sounds)
        {
            // on ajoute les variables du sound
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
    
    // play est utilisé pour jouer un son avec son nom
    public void Play(string name)
    {
        // il faut chercher un son avec le nom qu'on souhaite jouer
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Play();
    }

    public void Stop(string name)
    {
        // il faut chercher un son avec le nom qu'on souhaite jouer
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.Stop();
    }

    public void ResetSound()
    {
        foreach (Sound s in sounds)
        {
            s.source.Stop();
        }
    }

    // De la même façon on peut changer le volume du son
    public void ChangeVolume(string name, float amount)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        s.source.volume = amount;
    }

    public Sound GetSoundByName(string name)
    {
        int i = 0;
        bool find = false;

        Sound soundToReturn = null;

        while (!find && i < sounds.Length)
        {
            if (name == sounds[i].name)
            {
                soundToReturn = sounds[i];
            }

            ++i;
        }

        return soundToReturn;
    }

    /*
    public void LaunchAnother(string name, string name2)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        Sound s2 = Array.Find(sounds, sound => sound.name == name2);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }
        if (s2 == null)
        {
            Debug.LogWarning("Sound: " + name2 + " not found!");
            return;
        }
        if (s.)
        s.source.Stop();
        s2.source.Play();
    }
    */
}
