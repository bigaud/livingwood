﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    /// This player move with the mouse
    /// 

    /// this is the variable modifiable with player
    [SerializeField]
    private PlayerSetting player;

    [SerializeField]
    private Light lamp;

    private Ray mouseRay;
    private Vector3 positionMouseScreenWorld;
    [SerializeField]
    private Vector3 offset;
    private Quaternion rotationGoal;

    public ObstacleTrigger obstacle;

    public Animator animator;

    public int faggotPlayer;
        
    public float maxFaggot;

    public float speedFaggot;

    private int randomwalk;

    public bool isWalking;

    public void Start()
    {
        AudioManager.instance.Play("Wind");
        lamp.range = player.lightRange;
        GameManager.current.TriggerFagot += SetFaggot;
        GameManager.current.FuelFire += SetFaggot;
        GameManager.current.GameOver += DestroyYourself;
        speedFaggot = 1;
        maxFaggot = 5;
    }

    public void Update()
    {
        if (Input.GetAxis("Fire1")>0)
        {
            MovePlayer();
        }
        else
        {
            isWalking = false;
            animator.SetBool("Moving",false);
            CancelInvoke();
        }
    }

    public void MovePlayer()
    {
        mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, Mathf.Infinity, player.layerTouch))
        {
            if (!obstacle.colliding)
            {
                Debug.Log("stop");
                animator.SetBool("Moving", true);
                if (!isWalking)
                {
                    Invoke("walkingSound", 0.5f);
                }
                isWalking = true;
                transform.Translate(Vector3.forward * player.Speed* speedFaggot * Time.deltaTime);
            }
            else
            {
                CancelInvoke();
                isWalking = false;
                animator.SetBool("Moving", false);
            }
            //find the vector pointing from our position to the target
            positionMouseScreenWorld = (hit.point - transform.position).normalized;

            //create the rotation we need to be in to look at the target
            rotationGoal = Quaternion.LookRotation(positionMouseScreenWorld);

            //rotate us over time according to speed until we are in the required rotation
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(rotationGoal.eulerAngles+offset), Time.deltaTime * player.RotationSpeed);
        }
    }

    public void SetFaggot(int number)
    {
        
        faggotPlayer += number;
        speedFaggot = (maxFaggot - faggotPlayer) / maxFaggot;
    }
    
    public void walkingSound()
    {
        randomwalk = Random.Range(0, 3);
        switch (randomwalk)
        {
            case (0):
                AudioManager.instance.Play("Walk1");
                break;
            case (1):
                AudioManager.instance.Play("Walk2");
                break;
            case (2):
                AudioManager.instance.Play("Walk3");
                break;
            default:
                break;
        }
        Invoke("walkingSound", 0.5f);
    }

    public void DestroyYourself()
    {
        GameManager.current.GameOver -= DestroyYourself;
        CancelInvoke();
        gameObject.SetActive(false);
    }
}
