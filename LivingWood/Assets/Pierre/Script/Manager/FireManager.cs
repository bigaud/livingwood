﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireManager : MonoBehaviour
{

    public FireSetting fireSetting;

    private float currentFuel;
    [SerializeField]
    private Light fireLight, Torchlight;
    private float ratiolightfuel, ratiotorch;
    private int maxFaggotNeed;
    public FireTrigger FireFuelZone;


    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.Play("Fire");
        currentFuel = fireSetting.FuelMax;
        ratiolightfuel = fireLight.intensity / currentFuel;
        ratiotorch = Torchlight.intensity / currentFuel;
        maxFaggotNeed = Mathf.FloorToInt(currentFuel / fireSetting.RatioFuelFaggot);
        GameManager.current.FuelFire += addFuel;
    }

    // Update is called once per frame
    void Update()
    {
        AudioManager.instance.ChangeVolume("Fire", (currentFuel / fireSetting.FuelMax));
        AudioManager.instance.ChangeVolume("Wind", (1-currentFuel / fireSetting.FuelMax));
        currentFuel -= fireSetting.FuelDecreaseRate * Time.deltaTime;
        fireLight.intensity = currentFuel * ratiolightfuel;
        Torchlight.intensity = currentFuel * ratiotorch;
        FireFuelZone.faggotNeeded = maxFaggotNeed- Mathf.FloorToInt(currentFuel / fireSetting.RatioFuelFaggot);
        if (currentFuel<0)
        {
            GameManager.current.OnGameOver();
        }
    }

    void addFuel(int numberFaggot)
    {
        AudioManager.instance.Play("Burst");
        currentFuel += -numberFaggot * fireSetting.RatioFuelFaggot;
    }
}
