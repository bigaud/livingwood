﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager current;

    private void Awake()
    {
        current = this;

    }

    public event Action<int> TriggerFagot;
    public event Action<int> FuelFire;
    public event Action GameOver;

    public bool pickfag=false;
    public bool isClick = true;
    public bool fire = false;

    public GameObject goWood;
    public GameObject goFire;
    public GameObject goClick;

    public void FaggotTaken(int numberFag)
    {
        if (pickfag)
        {
            pickfag = false;
            goWood.SetActive(false);
            goFire.SetActive(true);
            fire=true;
        }
        if (TriggerFagot != null)
        {
            TriggerFagot(numberFag);
        }
    }

    public void FuelingFire(int numberFag)
    {
        if (fire)
        {
            goFire.SetActive(false);
            fire = false;
        }
        if (FuelFire != null)
        {
            FuelFire(numberFag);
        }
    }

    public void OnGameOver()
    {
        if (GameOver != null)
        {
            GameOver();
        }
        AudioManager.instance.Stop("Fire");
        AudioManager.instance.Stop("Wind");
    }

    public void Reset()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Update()
    {
        if (isClick && Input.GetAxis("Fire1") > 0)
        {
            goClick.SetActive(false);
            goWood.SetActive(true);
            isClick = false;
            pickfag = true;
        }
    }
}
