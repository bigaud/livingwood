﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTrigger : MonoBehaviour
{
    public bool colliding;

    private void OnTriggerStay(Collider other)
    {
        colliding = true;
    }

    public void OnTriggerExit(Collider other)
    {
        colliding = false;
    }
}
