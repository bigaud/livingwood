﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaggotTrigger : MonoBehaviour
{
    [SerializeField]
    public int numberFaggot = 1;
    private GameObject parent;

    public void Start()
    {
        parent = transform.parent.gameObject;
        GameManager.current.GameOver += DestroyYourself;
    }

    private void OnTriggerEnter(Collider other)
    {
        GameManager.current.FaggotTaken(numberFaggot);
        Destroy(parent);
    }

    public void DestroyYourself()
    {
        Destroy(gameObject);
    }

    public void OnDestroy()
    {
        GameManager.current.GameOver -= DestroyYourself;
    }
}
