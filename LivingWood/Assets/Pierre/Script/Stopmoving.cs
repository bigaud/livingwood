﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stopmoving : MonoBehaviour
{
    [SerializeField]
    float offset;
    private void Update()
    {
        transform.rotation = Quaternion.Euler(0,transform.parent.rotation.eulerAngles.y+offset,0);
    }
}
