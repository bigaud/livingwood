﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FireSetting : ScriptableObject
{
    public float FuelDecreaseRate;
    public float FuelMax;
    public float RatioFuelFaggot;
}
