﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerSetting : ScriptableObject
{
    public float Speed;
    public float RotationSpeed;
    public float lightRange;
    public LayerMask layerTouch;
}
